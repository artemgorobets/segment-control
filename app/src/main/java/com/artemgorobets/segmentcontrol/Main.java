package com.artemgorobets.segmentcontrol;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class Main extends Activity {

    Button button0;
    Button button1;
    Button button2;
    SegmentButton segmentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        segmentButton = (SegmentButton) findViewById(R.id.segment_btn);

        button0 = (Button) segmentButton.findViewWithTag("0");
        button0.setText("first");
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                segmentButton.setAction((String) button0.getTag());
                System.out.println(button0.getTag());
            }
        });


        button1 = (Button) segmentButton.findViewWithTag("1");
        button1.setText("second");
        button1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            segmentButton.setAction((String) button1.getTag());
            System.out.println(button1.getTag());
            }
        });

        button2 = (Button) segmentButton.findViewWithTag("2");
        button2.setText("third");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                segmentButton.setAction((String) button2.getTag());
                System.out.println(button2.getTag());
            }
        });

        segmentButton.toggleButtons("2");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}