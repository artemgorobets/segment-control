package com.artemgorobets.segmentcontrol;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by artemgorobets on 7/23/14.
 */
public class SegmentButton extends LinearLayout {

    private int mSetButtons = 2;
    private int mSegmentSelected = -1;


    public SegmentButton(Context context, AttributeSet attributes) {
        super(context, attributes);

        TypedArray array = context.getTheme().obtainStyledAttributes(
                attributes,
                R.styleable.SegmentButton,
                0, 0);

        try {
            mSetButtons = array.getInteger(R.styleable.SegmentButton_segmentCount, 2);
            mSegmentSelected = array.getInteger(R.styleable.SegmentButton_segmentSelected, -1);
        } finally {
            array.recycle();
        }

        setButtons(mSetButtons);
        if (mSegmentSelected >= 0) {
            toggleButtons(String.valueOf(mSegmentSelected));
        }
    }

    public void setAction(String tag) {
        toggleButtons(tag);
    }

    public void setButtons(int count) {

        for (int i = 0; i < count; i++) {
            LinearLayout layout = new LinearLayout(getContext());
            LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            Button button = new Button(getContext());
            LayoutParams buttonParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1);

            button.setText(String.valueOf(i));
            button.setTag(String.valueOf(i));

            this.addView(layout, layoutParams);
            layout.addView(button, buttonParams);
            Button btn = (Button) findViewWithTag(String.valueOf(i));

            if (i == 0 || i == count - 1) {
                if (i == 0) {
                    btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_first));
                } else if (i == count - 1) {
                    btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_last));
                }
            } else {
                btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_center));
            }
        }
    }

    private void resetButtons() {
        Button firstButton;
        Button lastButton;
        Button tmpButton;

        for (int i = 0; i < mSetButtons; i++) {

            tmpButton = (Button) findViewWithTag(String.valueOf(i));
            tmpButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_center_color));
            tmpButton.setTextColor(getResources().getColor(R.color.SegmentWhite));
        }

        firstButton = (Button) findViewWithTag("0");
        lastButton = (Button) findViewWithTag(String.valueOf(mSetButtons - 1));
        firstButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_first));
        firstButton.setTextColor(getResources().getColor(R.color.SegmentWhite));
        lastButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_last));
        lastButton.setTextColor(getResources().getColor(R.color.SegmentWhite));

    }

    public void toggleButtons(String tag) {

        resetButtons();

        for (int i = 0; i < mSetButtons; i++) {
            Button tmpBtn;
            tmpBtn = (Button) findViewWithTag(String.valueOf(i));

            if (i != Integer.valueOf(tag)) {

                if (i != 0 && i != mSetButtons - 1) {

                    tmpBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_center));
                    tmpBtn.setTextColor(getResources().getColor(R.color.SegmentBlue));

                } else {
                    if (i == 0) {
                        tmpBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_first));
                        tmpBtn.setTextColor(getResources().getColor(R.color.SegmentBlue));
                    } else if (i == mSetButtons - 1) {
                        tmpBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_last));
                        tmpBtn.setTextColor(getResources().getColor(R.color.SegmentBlue));
                    }
                }

            } else {
                if (i != 0 && i != mSetButtons - 1) {
                    tmpBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_center_color));
                    tmpBtn.setTextColor(getResources().getColor(R.color.SegmentWhite));
                } else if (i == 0) {
                    tmpBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_first_color));
                    tmpBtn.setTextColor(getResources().getColor(R.color.SegmentWhite));
                } else if (i == mSetButtons - 1) {
                    tmpBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_last_color));
                    tmpBtn.setTextColor(getResources().getColor(R.color.SegmentWhite));
                }

            }

            this.requestLayout();
        }
    }


}

